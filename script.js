/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?
2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати,
 що операція виконана успішно.

Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/ 


//1


let div1 = document.querySelector('.first-div');
let btn1 = document.querySelector('.first-btn');

function operation(){
    let text1 = document.createElement('p');
    text1.textContent = 'операція виконана успішно';
    div1.append(text1);
}

btn1.addEventListener('click', () => {setTimeout(operation,3000)});


//2

//Створюємо таймер зі значенням "10"
let timer = document.querySelector('.timer');
timer.textContent = '10';

//створюємо інтервал для зворотного відліку через кожну секунду
let go = setInterval(()=>{
    //задаємо зменшення контенту таймена на 1 з кожною ітерацією інтервала
    timer.textContent = parseInt(timer.textContent)-1;
    
    //зазначаємо завершення інтервалу після обнулення таймеру
    if(timer.textContent == '0') {

        //задаємо таймаут для того щоб 0 встиг зявитись перед оголошенням за 0,5 секунди
        setTimeout(()=>{timer.textContent = 'Зворотній відлік завершено'}, 500);

        //завершуємо інтервал
        clearInterval(go)
    }

}, 1000)






